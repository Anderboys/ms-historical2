package com.ander.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ander.model.Transaction;
import com.ander.repository.TransactionRepository;

@RestController
public class TransactionController {
	
	
	@Autowired
	TransactionRepository _transaRepository;
	
	@GetMapping("/transaction")
	public List<Transaction> getTransaction(){
		return _transaRepository.findAll();
	}
	

}
