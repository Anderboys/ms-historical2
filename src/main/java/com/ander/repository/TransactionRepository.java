package com.ander.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ander.model.Transaction;

public interface TransactionRepository extends MongoRepository<Transaction, String> {

}
